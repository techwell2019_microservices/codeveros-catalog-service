const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  duration: {
    type: Number,
    min: 1,
    max: 5,
    required: true
  },
  type: {
    type: String,
    enum: ['presentation', 'workshop', 'course'],
    required: true
  }
}, {
  toJSON: {
    versionKey: false
  }
});

module.exports = mongoose.model('Training', schema);
